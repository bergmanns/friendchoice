"""working with report processing: text tagging, segmentaion"""

from langdetect import detect, DetectorFactory


def leng_detect(input: str) -> str:
    DetectorFactory.seed = 0
    len = detect(input)
