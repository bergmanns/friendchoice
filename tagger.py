"""Working with Tags"""
from typing import Dict, List, NamedTuple

import db

class Tag(NamedTuple):
    """Tag structure"""
    id: str
    message_id: int
    name: str
    tag_type_id: int
    aliases: List[str]

class Tags:
    def __init__(self):
        self._tags = self._load_tags()

    def _load_tags(self) -> List[Tag]:
        """Возвращает справочник категорий расходов из БД"""
        tags = db.fetchall(
            "category", "codename name is_base_expense aliases".split()
        )
        categories = self._fill_aliases(categories)
        return categories
