"""I/O to database"""
import os
from typing import Dict, List, Tuple

import config as cfg
import sqlite3

conn = sqlite3.connect("choice.db")
cursor = conn.cursor()

def insert(table: str, column_values: Dict) -> int:
    columns = ', '.join( column_values.keys() )
    values = tuple(column_values.values())
    placeholders = ", ".join( "?" * len(column_values.keys()) )
    cursor.execute(f"INSERT INTO {table} ({columns}) VALUES ({placeholders})", values)
    conn.commit()
    last_id = cursor.lastrowid
    return last_id

def fetch(table: str, columns: Tuple) -> List:
    """Fetch specific columns from the table"""
    cols = ", ".join(columns)
    cursor.execute(f"SELECT {cols} FROM {table}")
    rows = cursor.fetchall()
    return rows

def fetch_issues(columns: str, chat_id: int, not_user_id: int) -> List:
    """Fetch specific rows and cols from the table"""
    #placeholders = ",".join("?" * len(values))
    cursor.execute(f"SELECT {columns} "
                    f"FROM reports "
                    f"WHERE chat_id={chat_id} "
                    f"AND report_type='issue' "
                    f"AND user_id!={not_user_id} "
                    f"ORDER BY created DESC "
                    f"LIMIT {cfg.last_issues_list_len}"
                    )
    rows = cursor.fetchall()
    return rows

def fetch_rows(table: str, columns: str, column_for: str, values: List) -> List:
    """Fetch specific rows and cols from the table"""
    placeholders = ",".join("?" * len(values))
    cursor.execute(f"SELECT {columns} FROM {table} WHERE {column_for} IN ({placeholders})", values)
    rows = cursor.fetchall()
    return rows

def check_id(table: str, col: str, val: str) -> int:
    cursor.execute(
        f"SELECT id FROM {table} WHERE {col} = ?",
        (val,))
    row = cursor.fetchall()
    if len(row)==0:
        id = 0
    else:
        id = row[0][0]
    return id


def fetchall(table: str, columns: List[str]) -> List[Tuple]:
    columns_joined = ", ".join(columns)
    cursor.execute(f"SELECT {columns_joined} FROM {table}")
    rows = cursor.fetchall()
    result = []
    for row in rows:
        dict_row = {}
        for index, column in enumerate(columns):
            dict_row[column] = row[index]
        result.append(dict_row)
    return result


def _init_db():
    """Init DB"""
    with open("createdb.sql", "r") as f:
        sql = f.read()
    cursor.executescript(sql)
    conn.commit()

def check_db_exists():
    """Check DB, init if not yet"""

    cursor.execute("SELECT name FROM sqlite_master "
                   "WHERE type='table' AND name='reports'")
    table_exists = cursor.fetchall()
    if table_exists:
        return
    _init_db()

check_db_exists()
