"""control bot interface"""
#Telegram bot server
#import

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import numpy as np
from typing import List
import config as cfg
import secret

import dflow
from dflow import User, Chat, Entity, Report


#apply token
TOKEN = secret.CHOICE_BOT_TOKEN
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


#greetings from bot
@dp.message_handler(commands=['start', 'help'])
async def send_welcome(msg: types.Message):
    """Bot sends greeting and help info, add chat to DB"""

    ext_chat = str(msg.chat.id)
    ext_user = str(msg.from_user.id)

    print(ext_chat)
    print(ext_user)
    if msg.chat.type == 'private':
        isbotchat = True
    else:
        isbotchat = False

    report_user = User(ext_user, msg.from_user.first_name, msg.from_user.last_name, msg.from_user.username)
    user_id = dflow.check_user(report_user)

    report_chat = Chat(ext_chat, msg.chat.username, msg.chat.type, msg.chat.title, isbotchat)
    chat_id = dflow.check_chat(report_chat)


    await msg.answer(
        "FriendChoice for community\n\n"
        "Report your issue to the community: /ask\n"
        "Committ a choice: /choice\n"
        "Help others with their issues, type in the chat with the bot: /ans\n")


@dp.message_handler(lambda msg: msg.text.startswith('/choice'))
async def record_choice(msg: types.Message):
    msg_ext_id = msg.message_id
    ext_user = str(msg.from_user.id)
    ext_chat = str(msg.chat.id)

    if msg.chat.type == 'private':
        isbotchat = True
        await bot.send_message(chat_id=ext_chat, text="Пока записываем рекомендации только в группах :) ")
    else:
        isbotchat = False

        report_user = User(ext_user, msg.from_user.first_name, msg.from_user.last_name, msg.from_user.username)
        user_id = dflow.check_user(report_user)

        report_chat = Chat(ext_chat, msg.chat.username, msg.chat.type, msg.chat.title, isbotchat)
        chat_id = dflow.check_chat(report_chat)

        raw_msg = msg.text[7:]

        report_choice = Report("choice",user_id,chat_id,msg_ext_id,raw_msg)
        try:
            dflow.write_report(report_choice)
        except BaseException as e:
            await msg.answer(str(e))
            return

        if msg.chat.username:
            link_to_chat = msg.chat.username
        else:
            if msg.chat.type == 'supergroup':
                link_to_chat = f"c/{ext_chat[4:]}"
            else:
                link_to_chat = f"c/{ext_chat[1:]}"

        link_to_m = f"https://t.me/{link_to_chat}/{msg_ext_id}"
        await bot.send_message(chat_id=msg.from_user.id,
                                text=f"<a href='{link_to_m}'>Your Choice with text: '<b>{raw_msg}</b>' is recorded</a>",
                                parse_mode='HTML')



@dp.message_handler(lambda msg: msg.text.startswith('/ask'))
async def record_issue(msg: types.Message):
    """Handle new issue and update db(users,chats,etc.)"""
    msg_ext_id = msg.message_id
    ext_user = str(msg.from_user.id)
    ext_chat = str(msg.chat.id)

    if msg.chat.type == 'private':
        isbotchat = True
        await bot.send_message(chat_id=ext_chat, text="Вопросы лучше задавать в community :) ")
    else:
        isbotchat = False

        report_user = User(ext_user, msg.from_user.first_name, msg.from_user.last_name, msg.from_user.username)
        user_id = dflow.check_user(report_user)

        report_chat = Chat(ext_chat, msg.chat.username, msg.chat.type, msg.chat.title, isbotchat)
        chat_id = dflow.check_chat(report_chat)

        raw_msg = msg.text[4:]

        report_ent = []
        for entity in msg.entities:
            ent_item = Entity(entity.type, entity.url, entity.user)
            report_ent.append(ent_item)

        report_issue = Report("issue",user_id,chat_id,msg_ext_id,raw_msg)

        try:
            dflow.write_report(report_issue)
        except BaseException as e:
            await msg.answer(str(e))
            return

        if msg.chat.username:
            link_to_chat = msg.chat.username
        else:
            if msg.chat.type == 'supergroup':
                link_to_chat = f"c/{ext_chat[4:]}"
            else:
                link_to_chat = f"c/{ext_chat[1:]}"

        link_to_m = f"https://t.me/{link_to_chat}/{msg_ext_id}"
        await bot.send_message(chat_id=msg.from_user.id,
                                text=f"<a href='{link_to_m}'>Your issue with text: '<b>{raw_msg}</b>' is recorded</a>",
                                parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data)
async def process_callback_chat(callback_query: types.CallbackQuery):
    """display links to recent issues in this chat"""

    (chat_id,callback_chat_id) = callback_query.data.split(",")

    #Get chat ext_chat_id or chat as an object
    chat = dflow.get_chat(chat_id)

    #Get current user to exclude his messages
    current_user_id = dflow.get_user_id_by_ext(callback_chat_id)

    #Get the ids of the messages
    issues = dflow.get_messages(chat_id,current_user_id)

    await bot.answer_callback_query(callback_query.id, text=f"Выбран чат {chat.title}")




    if chat.username:
            chat_public = True
            link_to_chat = chat.username
    else:
            chat_public = False
            if chat.type == 'supergroup':
                    link_to_chat = f"c/{chat.ext_chat_id[4:]}"
            else:
                    link_to_chat = f"c/{chat.ext_chat_id[1:]}"

    await bot.send_message(chat_id=callback_chat_id, text=f"Последние запросы в чате:")


    for issue in issues:
            user = dflow.get_user(issue.user_id)
            link_to_m = f"https://t.me/{link_to_chat}/{issue.msg_ext_id}"
            await bot.send_message(chat_id=callback_chat_id,
                                text=f"<a href='{link_to_m}'>{user.username} ({user.first_name},{user.last_name}) asked:'{issue.raw_text}'</a>",
                                parse_mode='HTML')



@dp.message_handler(commands='ans')
async def site_msg(msg: types.Message):
    """display the list of chats too choose last questions from"""
    ext_user = str(msg.from_user.id)
    ext_chat = str(msg.chat.id)
    if msg.chat.type == 'private':
        botchat_id = ext_chat
        report_user = User(ext_user, msg.from_user.first_name, msg.from_user.last_name, msg.from_user.username)
        rows = dflow.user_chat_list(report_user)

        keyboard = InlineKeyboardMarkup()
        for chat in rows:
            btn = InlineKeyboardButton(chat[1], callback_data=f"{chat[0]},{str(msg.chat.id)}")
            keyboard.add(btn)
        await bot.send_message(chat_id=msg.chat.id, text='Чаты, в которых была активность', reply_markup=keyboard)
    else:
        await bot.send_message(chat_id=msg.chat.id,
                                text=f"<a href='{cfg.link_to_chatbot}'>'Запросить список вопросов можно только лично у бота'</a>",
                                parse_mode='HTML',
                                disable_notification=True,
                                reply_to_message_id=msg.message_id)


@dp.message_handler(content_types=['text'])
async def get_text_messages(msg: types.Message):
   if msg.text.lower() == 'привет':
       await msg.answer('Привет, чувак!')
   else:
       await msg.answer('Не понимаю, что это значит.')

if __name__ == '__main__':
   executor.start_polling(dp)
