"""Working with user reports (messages,questions,choices)"""

from typing import List, NamedTuple, Optional

import datetime
import pytz
import db
import config

class Chat:
    def __init__(self,ch1,ch2,ch3,ch4,ch5):
        self.ext_chat_id = ch1
        self.username = ch2
        self.type = ch3
        self.title = ch4
        self.isbotchat = ch5

class User:
    def __init__(self,u1,u2,u3,u4):
        self.ext_user_id = u1
        self.first_name = u2
        self.last_name = u3
        self.username = u4

class Entity:
    def __init__(self,e1,e2,e3):
        self.type = e1
        self.url = e2
        self.user_id = e3

class Report:
    def __init__(self,r1,r2,r3,r4,r5):
        self.report_type = r1
        self.user_id = r2
        self.chat_id = r3
        self.msg_ext_id = r4
        self.raw_text = r5

def write_report(rep: Report) -> None:
    """Writes a new issue report from user"""

    report_row_id = db.insert("reports", {
        "report_type": rep.report_type,
        "created": _get_now_formatted(),
        "user_id": rep.user_id,
        "chat_id": rep.chat_id,
        "message_external_id": rep.msg_ext_id,
        "raw_text": rep.raw_text
    })

    if rep.report_type == 'choice':
        _write_choice(rep,report_row_id)

    #_write_entity(report_row_id, report_ent)

def read_issue() -> List:
    """Reruns a Reports from DB"""
    issues = db.fetch_issue("reports", ("message_external_id","raw_text"))
    return issues

def check_chat(report_chat: Chat) -> int:
    """Check if Chat exists in the db, create if not and return id"""
    id = db.check_id("chats","external_chat_id",report_chat.ext_chat_id)
    if id == 0:
        id = _add_chat(report_chat)
    return id

def check_user(report_user: User) -> int:
    """Check if Chat exists in the db, create if not and return id"""
    id = db.check_id("users","external_user_id",report_user.ext_user_id)
    if id == 0:
        id = _add_user(report_user)
    return id


def user_chat_list(report_user: User) -> List:
    """Returns a list of chats (titles) available for the user"""
    user_id = check_user(report_user)
    rows = db.fetch_rows("reports", "chat_id", "user_id", [user_id])
    chat_ids = [x[0] for x in rows]
    rows = db.fetch_rows("chats","id,title,isbotchat","id",chat_ids)
    nonbotrows = []
    for chat in rows:
        if not chat[2]:
            nonbotrows.append(chat)
    return nonbotrows

def get_chat(chat_id: int) -> Chat:
    """Returns the Chat object"""
    rows = db.fetch_rows("chats", "external_chat_id,username,type,title","id",[chat_id])
    chat = Chat(rows[0][0],rows[0][1],rows[0][2],rows[0][3],False)
    return chat

def get_user(user_id: int) -> User:
    """Returns User object"""
    rows = db.fetch_rows("users", "external_user_id,first_name,last_name,username", "id", [user_id])
    user = User(rows[0][0],rows[0][1],rows[0][2],rows[0][3])
    return user

def get_user_id_by_ext(external_user_id: str) -> int:
    """Returns User object"""
    rows = db.fetch_rows("users", "id", "external_user_id", [external_user_id])
    return rows[0][0]

def get_messages(chat_id: int, not_user_id: int) -> List:
    """Returns the messages in the chat"""
    rows = db.fetch_issues("*",chat_id,not_user_id)
    issues = []
    for row in rows:
        issues.append(Report(row[1],row[3],row[4],row[5],row[6]))
    return issues

def _write_choice(rep: Report, report_id: int) -> None:
    """Writes a new choice to db"""
    #process the text
    print(rep.raw_text)
    name = rep.raw_text
    contact = rep.raw_text

    #write to DB
    choice_id = db.insert("choices", {
            "user_id": rep.user_id,
            "report_id": report_id,
            "is_private": True,
            "name": name,
            "contact": contact,
            "created": _get_now_formatted(),
            })

def _write_entity(report_row_id: int, report_ent: List[Entity]) -> None:
    """Writes entities from the report to DB"""
    for ent in report_ent:
        #print(ent.type, ent.user, ent.url)
        if ent.type == 'text_mention':
            ent_user = User(ent.user.id, ent.user.first_name, ent.user.last_name, ent.user.username)
            ent_user_id = _check_user(ent_user)

            ent_row_id = db.insert("entities", {
            "type": ent.type,
            "report_id": report_row_id,
            "created": _get_now_formatted(),
            "url": ent.url,
            "mention_user_id": ent_user_id
            })
        else:
            ent_row_id = db.insert("entities", {
            "type": ent.type,
            "report_id": report_row_id,
            "created": _get_now_formatted(),
            "url": ent.url,
            #"mention_user_id": ent.user
            })


def _add_user(new_user: User) -> int:
    "Add new user"
    id = db.insert("users", {
            "external_user_id": new_user.ext_user_id,
            "first_name": new_user.first_name,
            "last_name": new_user.last_name,
            "username": new_user.username,
            "created": _get_now_formatted()
    })
    return id

def _add_chat(new_chat: Chat) -> int:
    "Add new chat"
    id = db.insert("chats", {
            "external_chat_id": new_chat.ext_chat_id,
            "username": new_chat.username,
            "type": new_chat.type,
            "title": new_chat.title,
            "isbotchat": new_chat.isbotchat,
            "created": _get_now_formatted()
    })
    return id


def _get_now_formatted() -> str:
    """Returns current date as a string"""
    return _get_now_datetime().strftime("%Y-%m-%d %H:%M:%S")

def _get_now_datetime() -> datetime.datetime:
    """Returns date in Msk time zone"""
    tz = pytz.timezone("Europe/Moscow")
    now = datetime.datetime.now(tz)
    return now
