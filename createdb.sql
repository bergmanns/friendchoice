create table reports(
    id integer primary key,
    report_type varchar(255),
    created datetime,
    user_id integer,
    chat_id integer,
    message_external_id integer,
    raw_text text,
    language text,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (chat_id) REFERENCES chats(id)
);

create table entities(
    id integer primary key,
    type varchar(255),
    report_id integer,
    created datetime,
    url text,
    mention_user_id integer,
    FOREIGN KEY (report_id) REFERENCES reports(id)
);

create table choices(
    id integer primary key,
    user_id integer,
    report_id integer,
    is_private boolean,
    name text,
    contact text,
    created datetime,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (report_id) REFERENCES reports(id)
);

create table tags(
    id integer primary key,
    report_id integer,
    codename text,
    aliases text,
    FOREIGN KEY (report_id) REFERENCES reports(id)
);

create table users(
    id integer primary key,
    external_user_id text,
    first_name text,
    last_name text,
    username text,
    phone text,
    created datetime
);

create table chats(
    id integer primary key,
    external_chat_id text,
    username text,
    type text,
    title text,
    isbotchat boolean,
    created datetime
);
