## Project description

The initial idea is to structure recommendations of your community so that you can rapidly and asynchronously access their choice for your question.

Today we tend to look for an advices from our communities (Facebook newsfeed, Telegram groups, WhatsApp chats)
The scenario to find out an advice to smth from the community: make a post/message and get replies/comments:

The pain 
- not structured in chat history,
- half synchronous = needs attention of others in the nearest time
- the questions tend to be asked repeatedly
- no statistics/analytics of opinions
- hard to help community proactively
- no access outside the group

## Current solution (Work in Progress)

FriendChoice bot
- A telegram bot that can catch people's questions and choices and write it to the database
- Possibility to quickly access people choices via Telegram 
